package com.zaky.inventaris.dashboard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.ktx.Firebase
import com.google.firebase.firestore.ktx.firestore

import com.zaky.inventaris.R
import com.zaky.inventaris.search.SearchActivity

class DashboardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        val fabDashboard = findViewById<FloatingActionButton>(R.id.fabDashboard)
        fabDashboard.setOnClickListener {
            val i = Intent(this, SearchActivity::class.java)
            startActivity(i)
            finish()
        }


    }


}