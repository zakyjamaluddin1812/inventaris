package com.zaky.inventaris.search

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.zaky.inventaris.R

class SearchViewModel : ViewModel() {
    private val _inventaris = MutableLiveData<ArrayList<Inventaris>>()
    val inventaris : LiveData<ArrayList<Inventaris>> = _inventaris
    val dataInv = ArrayList<Inventaris>()
    init {
        getData()
    }

    private fun getData() {
        val db = Firebase.firestore

        db.collection("inventaris")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    dataInv.add(Inventaris(document.data.get("nama").toString(), document.data.get("deskripsi").toString(),document.data.get("gambar2").toString(),document.data.get("gambar").toString(),))
                }
                _inventaris.value = dataInv
                Log.d("TAG", "getData: $dataInv")
            }
            .addOnFailureListener { exception ->
                Log.d("TAG", "Error getting documents: ", exception)
            }



    }
}