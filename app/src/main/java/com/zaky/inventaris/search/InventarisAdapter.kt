package com.zaky.inventaris.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zaky.inventaris.R

class InventarisAdapter (private val listInventaris : List<Inventaris>, private val context : AppCompatActivity) : RecyclerView.Adapter<InventarisAdapter.InventarisViewHolder>() {
    class InventarisViewHolder (itemView : View) : RecyclerView.ViewHolder(itemView) {
        val gambar : ImageView = itemView.findViewById(R.id.gambarInventaris)
        val nama : TextView = itemView.findViewById((R.id.namaInventaris))
        val deskripsi : TextView = itemView.findViewById((R.id.deskripsiInventaris))

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InventarisViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_inventaris, parent, false)
        return  InventarisViewHolder(view)
    }

    override fun onBindViewHolder(holder: InventarisViewHolder, position: Int) {
        val inventaris = listInventaris[position]

        Glide.with(context)
            .load(inventaris.gambar2)
            .into(holder.gambar);
        holder.nama.text = inventaris.nama
        holder.deskripsi.text = inventaris.deskripsi
    }

    override fun getItemCount(): Int {
        return listInventaris.size
    }
}