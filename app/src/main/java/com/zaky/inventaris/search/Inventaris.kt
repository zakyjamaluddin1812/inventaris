package com.zaky.inventaris.search

data class Inventaris(
    val nama : String,
    val deskripsi : String,
    val gambar2 : String,
    val gambar : String,
)
