package com.zaky.inventaris.search

import android.content.Intent
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.zaky.inventaris.R
import com.zaky.inventaris.add.AddActivity

class SearchActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var listInvetaris : ArrayList<Inventaris>
    private lateinit var inventarisAdapter: InventarisAdapter
    private lateinit var progressBar : ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        progressBar = findViewById(R.id.progressSearch)
        val fabSearch = findViewById<FloatingActionButton>(R.id.fabSearch)
        fabSearch.setOnClickListener {
            val i = Intent(this, AddActivity::class.java)
            startActivity(i)
            finish()

        }
        listInvetaris = ArrayList()

        val mainViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(SearchViewModel::class.java)
        mainViewModel.inventaris.observe(this, { inventaris ->
            listInvetaris = inventaris
            initRv(listInvetaris)

            Log.d("TAG", "onCreate: $listInvetaris")
        })




    }

    private fun initRv(listInventarisObserver: ArrayList<Inventaris>) {

        recyclerView = findViewById(R.id.rvInventaris)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        inventarisAdapter = InventarisAdapter(listInventarisObserver, this)
        recyclerView.adapter = inventarisAdapter
        progressBar.visibility = View.GONE

    }

}