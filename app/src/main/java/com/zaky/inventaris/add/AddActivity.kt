package com.zaky.inventaris.add

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_GET_CONTENT
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.EditText
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import com.zaky.inventaris.R
import com.zaky.inventaris.search.Inventaris

import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class AddActivity : AppCompatActivity() {
    private lateinit var fotoAdd : ImageView
    private lateinit var fileFoto : File
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)

        val fabAdd = findViewById<FloatingActionButton>(R.id.fabAdd)
        fotoAdd = findViewById(R.id.fotoAdd)
        fabAdd.setOnClickListener {

            uploadImage(fileFoto).toString()

        }
        val btnCameraAdd = findViewById<ImageView>(R.id.btnCameraAdd)
        btnCameraAdd.setOnClickListener {
            startGallery()
        }
    }

    private fun upload (nama : String, kategori : String, gambar: String) {
        val db = Firebase.firestore
        val inventaris = Inventaris(nama, kategori, gambar, "Berhail")
        db.collection("inventaris")
            .add(inventaris)
            .addOnSuccessListener { documentReference ->
                Log.d("TAG", "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w("TAG", "Error adding document", e)
            }
    }

    private val launcherIntentGallery = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == RESULT_OK) {
            val selectedImg: Uri = result.data?.data as Uri
            val myFile = uriToFile(selectedImg, this@AddActivity)
            Log.d("TAG", "selectedImg:  $selectedImg, $myFile")

            fotoAdd.setImageURI(selectedImg)
            fileFoto = myFile
        }
    }

    private fun startGallery() {
        val intent = Intent()
        intent.action = ACTION_GET_CONTENT
        intent.type = "image/*"
        val chooser = Intent.createChooser(intent, "Choose a Picture")
        launcherIntentGallery.launch(chooser)
    }




    private fun uploadImage(file: File) {
        val storage = Firebase.storage
        var storageRef = storage.reference
        var file = Uri.fromFile(file)
        var imagesRef: StorageReference? = storageRef.child("images/${file.lastPathSegment}")
        val uploadTask = imagesRef?.putFile(file)

        val uri = uploadTask?.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            imagesRef?.downloadUrl
        }?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val nama = findViewById<EditText>(R.id.namaAdd).text.toString()
                val kategori = findViewById<EditText>(R.id.kategoriAdd).text.toString()
                val gambar = task.result.toString()
                upload(nama, kategori, gambar)

            } else {
            }
        }

    }


    private val FILENAME_FORMAT = "dd-MMM-yyyy"

    val timeStamp: String = SimpleDateFormat(
        FILENAME_FORMAT,
        Locale.US
    ).format(System.currentTimeMillis())
    fun createCustomTempFile(context: Context): File {
        val storageDir: File? = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(timeStamp, ".jpg", storageDir)
    }

    fun uriToFile(selectedImg: Uri, context: Context): File {
        val contentResolver: ContentResolver = context.contentResolver
        val myFile = createCustomTempFile(context)

        val inputStream = contentResolver.openInputStream(selectedImg) as InputStream
        val outputStream: OutputStream = FileOutputStream(myFile)
        val buf = ByteArray(1024)
        var len: Int
        while (inputStream.read(buf).also { len = it } > 0) outputStream.write(buf, 0, len)
        outputStream.close()
        inputStream.close()

        return myFile
    }
}